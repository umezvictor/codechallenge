﻿using System.Threading.Tasks;
using TestProject.ViewModels;

namespace TestProject.Services.AuthService
{
    public interface IAuthService
    {
        Task<ApiResponse<string>> AuthenticateUser(LoginVM loginVM);
    }
}