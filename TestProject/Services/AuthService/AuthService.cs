﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestProject.Entities;
using TestProject.Responses;
using TestProject.ViewModels;

namespace TestProject.Services.AuthService
{
    public class AuthService : IAuthService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly ILogger _logger;

        public AuthService(UserManager<User> userManager, SignInManager<User> signInManager, ILogger<AuthService> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
        }



        public async Task<ApiResponse<string>> AuthenticateUser(LoginVM loginVM)
        {
            ApiResponse<string> apiResponse = new ApiResponse<string>();
            try
            {

                //check if user exists
                var user = await _userManager.FindByEmailAsync(loginVM.Email.Trim());
                if (user == null)
                {
                    apiResponse.Message = ResponseMessage.InvalidEmailOrPassword;
                    apiResponse.IsSuccessful = false;
                    return apiResponse;
                }


                var authResponse = await _signInManager.PasswordSignInAsync(loginVM.Email.Trim(), loginVM.Password.Trim(), false, false);
                if (authResponse.Succeeded)
                {
                    //return user object in payload                   
                    apiResponse.IsSuccessful = true;
                    apiResponse.Payload = JsonConvert.SerializeObject(user);
                    return apiResponse;

                }
                apiResponse.Message = ResponseMessage.InvalidEmailOrPassword;
                apiResponse.IsSuccessful = false;
                return apiResponse;
            }
            catch (Exception ex)
            {
                _logger.LogError("ERROR AUTHENTICATING USER " + ex.Message);
                apiResponse.Message = ResponseMessage.Error;
                apiResponse.IsSuccessful = false;
                return apiResponse;
            }

        }
    }
}
