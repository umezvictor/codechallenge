﻿using System.Collections.Generic;
using System.Security.Claims;

namespace TestProject.Services.TokenService
{
    public interface ITokenService
    {
        string GenerateAccessToken(IEnumerable<Claim> claims);
       
    }
}