﻿using System.Threading.Tasks;
using TestProject.ViewModels;

namespace TestProject.Services.UserService
{
    public interface IUserService
    {
        Task<ApiResponse<string>> CreateUser(UserVM userVM);
    }
}