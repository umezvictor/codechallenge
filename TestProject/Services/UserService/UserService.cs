﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestProject.Entities;
using TestProject.Responses;
using TestProject.ViewModels;

namespace TestProject.Services.UserService
{
    public class UserService : IUserService
    {

        private readonly UserManager<User> _userManager;
        private readonly ILogger _logger;

        public UserService(UserManager<User> userManager, ILogger<UserService> logger)
        {

            _userManager = userManager;
            _logger = logger;
        }


        public async Task<ApiResponse<string>> CreateUser(UserVM userVM)
        {
            ApiResponse<string> apiResponse = new ApiResponse<string>();
            try
            {
                //check if user exists
                var existingUser = await _userManager.FindByEmailAsync(userVM.Email);
                if (existingUser != null)
                {
                    apiResponse.IsSuccessful = false;
                    apiResponse.Message = ResponseMessage.UserExists;
                    return apiResponse;
                }

                //create user object
                var user = new User
                {
                    Email = userVM.Email.Trim(),
                    UserName = userVM.Email.Trim(),
                    FirstName = userVM.FirstName.Trim(),
                    LastName = userVM.LastName.Trim(),
                    PhoneNumber = userVM.Phone.Trim(),
                    City = userVM.City.Trim(),
                    Country = userVM.Country.Trim()
                };
                //save user
                var creatingUser = await _userManager.CreateAsync(user, userVM.Password.Trim());
                if (creatingUser.Succeeded)
                {
                    apiResponse.IsSuccessful = true;
                    apiResponse.Message = ResponseMessage.AccountCreated;
                    return apiResponse;
                }


                apiResponse.IsSuccessful = false;
                apiResponse.Message = ResponseMessage.OperationFailed;
                return apiResponse;

            }
            catch (Exception ex)
            {
                _logger.LogError("ERROR CREATING USER " + ex.Message);
                apiResponse.IsSuccessful = false;
                apiResponse.Message = ResponseMessage.Error;
                return apiResponse;
            }
        }

    }
}

