﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using TestProject.Entities;
using TestProject.Responses;
using TestProject.Services.AuthService;
using TestProject.Services.TokenService;
using TestProject.ViewModels;

namespace TestProject.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        private readonly ILogger<AuthController> _logger;
        private readonly ITokenService _tokenService;

        public AuthController(IAuthService authService, ILogger<AuthController> logger, ITokenService tokenService)
        {

            _authService = authService;
            _logger = logger;
            _tokenService = tokenService;
        }



        [HttpPost]
        [ProducesResponseType(typeof(LoginResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Login([FromBody]LoginVM loginVM)
        {
            LoginResponse loginResponse = new LoginResponse();
            try
            {
                if (!ModelState.IsValid)
                {
                    loginResponse.Message = ResponseMessage.BadRequest;
                    loginResponse.IsSuccessful = false;
                    loginResponse.StatusCode = 400;
                    return BadRequest(loginResponse);
                }

                //authenticate user
                var authResponse = await _authService.AuthenticateUser(loginVM);

                if (authResponse.IsSuccessful)
                {
                    //retrieve user info from payload
                    var user = JsonConvert.DeserializeObject<User>(authResponse.Payload);
                    if (user != null)
                    {
                        //if ok, fetches some user details and stores in claims. Can be used by frontend app
                        List<Claim> claims = new List<Claim>();
                        var email = new Claim("Email", user.Email);
                        var userId = new Claim("Id", user.Id.ToString());
                        var name = new Claim("Name", user.FirstName);

                        claims.Add(email);
                        claims.Add(userId);
                        //generate token for user
                        var token = _tokenService.GenerateAccessToken(claims);

                        loginResponse.Token = token;
                        loginResponse.FirstName = user.FirstName;
                        loginResponse.LastName = user.LastName;
                        loginResponse.Email = user.Email;
                        loginResponse.Phone = user.PhoneNumber;

                        loginResponse.Message = ResponseMessage.Success;
                        loginResponse.IsSuccessful = true;
                        loginResponse.StatusCode = 200;                        
                        return Ok(loginResponse);
                    }

                    //if user is null
                    loginResponse.Message = authResponse.Message;
                    loginResponse.IsSuccessful = false;
                    loginResponse.StatusCode = 400;
                    return BadRequest(loginResponse);
                }
                //if authentication fails
                loginResponse.Message = authResponse.Message;
                loginResponse.IsSuccessful = false;
                loginResponse.StatusCode = 400;
                return BadRequest(loginResponse);

            }
            catch (Exception ex)
            {
                _logger.LogError("LOGIN ERROR " + ex.Message);
                loginResponse.Message = ResponseMessage.Error;
                loginResponse.IsSuccessful = false;
                loginResponse.StatusCode = 500;
                return StatusCode(500, loginResponse);

            }

        }
    }


}
