﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TestProject.Responses;
using TestProject.Services.UserService;
using TestProject.ViewModels;

namespace TestProject.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ILogger<UserController> _logger;

        public UserController(IUserService userService, ILogger<UserController> logger)
        {
            _userService = userService;
            _logger = logger;
        }


        [HttpPost]
        [ProducesResponseType(typeof(ApiResponse<string>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create([FromBody] UserVM userVM)
        {
            ApiResponse<string> apiResponse = new ApiResponse<string>();

            try
            {
                if (!ModelState.IsValid)
                {
                    apiResponse.Message = ResponseMessage.BadRequest;
                    apiResponse.IsSuccessful = false;
                    apiResponse.StatusCode = 400;
                    return BadRequest(apiResponse);
                }

                var result = await _userService.CreateUser(userVM);
                if (result.IsSuccessful)
                {
                    apiResponse.Message = result.Message;
                    apiResponse.IsSuccessful = result.IsSuccessful;
                    apiResponse.StatusCode = 201;
                    return Ok(apiResponse);
                }

                apiResponse.Message = result.Message;
                apiResponse.IsSuccessful = result.IsSuccessful;
                apiResponse.StatusCode = 400;
                return BadRequest(apiResponse);
            }
            catch (Exception ex)
            {

                _logger.LogError("ERROR CREATING USER " + ex.Message);
                apiResponse.Message = ResponseMessage.Error;
                apiResponse.IsSuccessful = false;               
                apiResponse.StatusCode = 500;               
                return StatusCode(500, apiResponse);
               
            }
            
        }
    }
}
