﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestProject.Responses
{
    public class LoginResponse
    {
       
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Token { get; set; }

        public string Message { get; set; }
        public int StatusCode { get; set; }
       
        public bool IsSuccessful { get; set; }


    }
}
