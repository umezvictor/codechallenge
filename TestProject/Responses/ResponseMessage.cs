﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestProject.Responses
{
    public class ResponseMessage
    {
      
        public static string BadRequest = "Invalid request";       
        public static string Error = "An internal server error occured";
        public static string RecordNotFound = "Record not found";
        public static string UserExists = "User already exists";       
        public static string InvalidEmailOrPassword = "Invalid email or password";      
        public static string AccountCreated = "Account created successfully";      
        public static string FieldsRequired = "All fields are required";
        public static string OperationFailed = "Operation failed";
        public static string Success = "Successful";

    }
}
