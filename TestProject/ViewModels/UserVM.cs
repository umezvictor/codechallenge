﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TestProject.ViewModels
{
    public class UserVM
    {

        [Required(ErrorMessage = "First name is required")]
        [DataType(DataType.Text)]
        [StringLength(30, MinimumLength = 3)]
        public string FirstName { get; set; }

        [DataType(DataType.Text)]
        [StringLength(30, MinimumLength = 3)]
        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; }


        //must be 11 digits
        [Required(ErrorMessage = "You must provide a phone number")]
        [RegularExpression(@"^(\d{11})$", ErrorMessage = "Invalid phone number, must be 11 digits")]
        public string Phone { get; set; }

        [Required]
        //[StringLength(128)]
        [EmailAddress]
        public string Email { get; set; }


        [Required]
        [StringLength(18, ErrorMessage = "Password must be at least 8 characters long.", MinimumLength = 8)]
        //uncomment to test password rule: [RegularExpression(@"^((?=.*[a-z])(?=.*[A-Z])(?=.*\d)).+$", ErrorMessage = "Password must contain at least one uppercase letter, one lowercase letter, one number")]
        [DataType(DataType.Password)]
        public string Password { get; set; }



        public string Country { get; set; }
        public string City { get; set; }


    }
}
